// a.)
// Element mit Id information selektieren
const elBookInfo = document.getElementById('information');
elBookInfo.classList.add('box');

// b.)
// Alle Elemente mit Tag h1 selektieren => Liste
const elTitlesH1 = document.getElementsByTagName('h1');
// Klasse darkblue an 1. Element aus selektierten Liste hinzufügen
// (es gibt nur ein Element in Liste, weil html nur h1 enthält)
elTitlesH1[0].classList.add('darkblue');

// c.)
// Alle Elemente mit der Klasse intro selektieren => Liste
const elIntros = document.getElementsByClassName('intro');
// Allen Elementen Klasse grey hinzufügen
for (let i = 0; i < elIntros.length; i++) {
  const elIntro = elIntros[i];
  elIntro.classList.add('grey');
}

// d.)
// Alle Elemente mit Tag span selektieren, die zudem Klasse
// javascript enthalten => Liste
const elJavaScripts = document.querySelectorAll('span.javascript');
// Allen Elementen Klasse small-box hinzufügen
for (let i = 0; i < elJavaScripts.length; i++) {
  const elJavaScript = elJavaScripts[i];
  elJavaScript.classList.add('small-box');
}

// e.)
// Alle Elemente im Inhaltsverzeichnis (Elemente mit Id table-of-content)
// mit Klasse info selektieren => Liste
const elInfosToc = document.querySelectorAll('#table-of-content .info');
// Allen Elementen Klasse info-sign hinzufügen
for (let i = 0; i < elInfosToc.length; i++) {
  const elInfoToc = elInfosToc[i];
  elInfoToc.classList.add('info-sign');
}

// f.)
// Alle ungeraden li-Elemente der Unterkapitel selektieren => Liste
const elEvenListItemsToc = document.querySelectorAll(
  '#table-of-content ul li:nth-child(even)'
);
// Allen Elementen Klasse stripe-light hinzufügen
for (let i = 0; i < elEvenListItemsToc.length; i++) {
  const elEvenListItemToc = elEvenListItemsToc[i];
  elEvenListItemToc.classList.add('stripe-light');
}

// g.)
// Letztes li-Element der Unterkapitel selektieren => Liste
const elSummaryItemsToc = document.querySelectorAll(
  '#table-of-content ul li:nth-last-child(1)'
);
// Allen Elementen Klasse italic hinzufügen
for (let i = 0; i < elSummaryItemsToc.length; i++) {
  const elSummaryItemToc = elSummaryItemsToc[i];
  elSummaryItemToc.classList.add('italic');
}

// h.)
// Erstes Element mit Klasse intro selektieren
const elFirstIntro = document.querySelector('.intro');
// Klasse grey entfernen
elFirstIntro.classList.remove('grey');
